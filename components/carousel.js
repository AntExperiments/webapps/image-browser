import { useEffect, useState } from 'react'
import styles from '../styles/Carousel.module.css'

export default function Carousel({ files, openFile, close }) {
    const [ currentIndex, setCurrentIndex ] = useState(files.indexOf(openFile))

    const goBack = () => {
        const newIndex = --currentIndex
        setCurrentIndex(newIndex < 0 ? files.length - 1 : newIndex)
    }

    const goForward = () => {
        const newIndex = ++currentIndex
        setCurrentIndex(newIndex > files.length - 1? 0 : newIndex)
    }

    useEffect(() => {
        window.onkeydown = ({ keyCode }) => {
            switch (keyCode) {
                case 37:
                    goBack()
                    break;
                case 39:
                    goForward()
                    break;
                case 8:
                    close()
                    break;
            }
        }
    })

    return <main className={styles.carousel}>
        <a onClick={goBack}>
            <svg viewBox="0 0 24 24">
                <path fill="currentColor" d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z" />
            </svg>
        </a>

        <img src={files[currentIndex]} alt="" />

        <a onClick={goForward}>
            <svg viewBox="0 0 24 24">
                <path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
            </svg>
        </a>

        <a onClick={close} className={styles.closeButton}>
            <svg viewBox="0 0 24 24">
                <path fill="currentColor" d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
            </svg>
        </a>
        <img style={{display: "none"}} src={files[currentIndex - 1 < 0 ? files.length - 1 : currentIndex - 1]}/>
        <img style={{display: "none"}} src={files[currentIndex + 1 > files.length - 1 ? 0 : currentIndex + 1]}/>
    </main>
}