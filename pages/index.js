import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useState } from 'react'
import fs from 'fs'
import Carousel from '../components/carousel'
import initializeBasicAuth from 'nextjs-basic-auth'
import base64 from 'base-64'

export default function Home({ files, path }) {
  const [ carouselFiles, setCarouselFiles ] = useState([null, null])

  const closeCarousel = () => {
    setCarouselFiles([null, null])
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Camp Buddy Archive</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {carouselFiles[0] && <Carousel files={carouselFiles[0]} openFile={carouselFiles[1]} close={closeCarousel} />}

      <main className={styles.main}>

        <div className={styles.breadcrumbs}>
          <a href="/">Home</a>
          {[path].map(p => {
            const matches = path.match(/[^\/]+/g)
            if (!matches) return ""
            return matches.map((match, i) => <div key={`${match}${i}`}>
              <span>/</span>
              <a href={`/?p=${matches.length - 1 == i ? path : matches.slice(0, i - matches.length + 1).join("/")}`}>{match}</a>
            </div>)
          })}
        </div>

        <div className={styles.grid}>

          {files.map(({ name, cover, isDirectory }) => {
            if (isDirectory) {
              return <a key={name} href={`/?p=${encodeURIComponent(`${path}${path ? "/" : ""}${name}`)}`} className={styles.card}>
                <img src={`/api/img?p=${encodeURIComponent(cover)}`} alt="" />
                <p>{name}</p>
              </a>
            } else {
              return <a key={name} className={styles.card}
                onClick={() => {
                  setCarouselFiles([files.filter(f => !f.isDirectory).map((({ cover }) => cover)), cover])
                }}
              >
                <img src={`/api/img?p=${encodeURIComponent(cover)}`} alt="" />
                <p>{name}</p>
              </a>
            }
          }
          )}
        </div>

      </main>
    </div>
  )
}

export async function getServerSideProps ({ query, req, res }) {
  const users = JSON.parse(fs.readFileSync("users.json", "utf8"))

  initializeBasicAuth({
    users
  })(req, res)

  if (!users.some(({ user, password }) => base64.encode(`${user}:${password}`) == req.headers.authorization.split(" ")[1])) {
    res.end('<html>Unauthorized</html>')
  }

  // TODO: Ignore transparent folder
  const findFirstImage = subpath => new Promise(resolve => {
    const deepSearch = subpath => {
      if (subpath.endsWith(".jpg") || subpath.endsWith(".png") || subpath.endsWith(".webp"))
        resolve(subpath)

      fs.readdirSync(subpath).filter(f => !f.startsWith(".")).map(file => {
        const path = `${subpath}/${file}`
        if (file.endsWith(".jpg") || file.endsWith(".png") || file.endsWith(".webp"))
          resolve(path)
        else{
          return deepSearch(path)}
      })

      resolve(null)

    }
    deepSearch(subpath)
  })

  const basePath = `public/images/${query.p || ""}`

  let files = fs.readdirSync(basePath).filter(f => !f.startsWith("."))
  if (!query.p)
    files = files.reverse()
  for (const i in files) {
    files[i] = {
      name: files[i],
      cover: (await findFirstImage(`${basePath}/${files[i]}`)).replace("public", ""),
      isDirectory: !(files[i].endsWith(".jpg") || files[i].endsWith(".png") || files[i].endsWith(".webp"))
    }
  }

  return {
    props: {
      files,
      path: query.p || ""
    }
  }
}
