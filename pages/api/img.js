import sharp from 'sharp'
import fs from 'fs'
import { createHash } from 'crypto'

const dir = "cache"

  // todo make save
export default async function handler(req, res) {
  const { p } = req.query

  if (!fs.existsSync(dir))
    fs.mkdirSync(dir)

  const hash = createHash('md5').update(p).digest('hex')

  // does thumbnail already exist?
  const file = `${dir}/${hash}.webp`
  if (!fs.existsSync(file)) {
    console.log("Generating new file for", p)
    await sharp(fs.readFileSync(`public/${p}`))
    .resize(572, 429)
    .toFile(`${dir}/${hash}.webp`)
  }
  res.setHeader('Content-Type', 'image/webp')
  res.status(200).send(fs.readFileSync(file))
}
