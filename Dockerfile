FROM node:lts-alpine
WORKDIR /app
COPY . .
RUN npm i
RUN npm run build
EXPOSE 3000
ENTRYPOINT ["npm"]
CMD ["start"]